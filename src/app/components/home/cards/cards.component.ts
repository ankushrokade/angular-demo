import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css'],
})
export class CardsComponent implements OnInit {
  @Input() postData: any = [];
  @Output() clickId = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  test(id: number) {
    this.clickId.emit(id);
  }
}
